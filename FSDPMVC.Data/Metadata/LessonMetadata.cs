﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FSDPMVC.Data.Metadata
{
    class LessonMetadata
    {
        public int LessonID { get; set; }
        [Required]
        [Display(Name = "Lesson Title")]
        public string LessonTitle { get; set; }

        public int CourseID { get; set; }

        public string Introduction { get; set; }
        [Display(Name = "Video URL")]
        public string VideoURL { get; set; }
        [Display(Name = "PDF File Name")]
        public string PdfFileName { get; set; }

        public bool IsActive { get; set; }


    }
    [MetadataType(typeof(LessonMetadata))]
    public partial class Lesson { }
}
