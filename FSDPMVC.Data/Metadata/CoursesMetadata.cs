﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace FSDPMVC.Data.Metadata
{
    public class CoursesMetadata
    {

        public int CourseID { get; set; }

        [Display(Name = "Course Name")]
        public string CourseName { get; set; }

        [UIHint("MultilineText")]
        public string Description { get; set; }


        public bool IsActive { get; set; }

    }
    [MetadataType(typeof(CoursesMetadata))]
    public partial class Cours { }


}
