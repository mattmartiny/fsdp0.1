﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace FSDPMVC.Data.Metadata
{
    public class LessonViewMetadata
     {
        public int LessonViewID { get; set; }

        public string UserID { get; set; }
        public int LessonID { get; set; }

        public System.DateTime DateViewed { get; set; }
        public virtual Lesson Lesson { get; set; }

    }
    [MetadataType(typeof(LessonViewMetadata))]
     public partial class LessonView{ }
}
