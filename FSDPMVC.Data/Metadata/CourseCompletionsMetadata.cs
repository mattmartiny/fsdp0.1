﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FSDPMVC.Data.Metadata
{
    class CourseCompletionsMetadata
    {
        public int CourseCompletionID { get; set; }

        public string UserID { get; set; }

        public int CourseID { get; set; }

        public System.DateTime DateCompleted { get; set; }

    }

[MetadataType(typeof(CourseCompletionsMetadata))]
public partial class CourseCompletions { }
}
